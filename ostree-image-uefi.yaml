{{ $architecture := or .architecture "amd64" }}
{{ $type := or .type "fixedfunction" }}
{{ $suite := or .suite "v2023dev2" }}
{{ $image := or .image (printf "apertis-ostree-%s-%s-%s" $suite  $type $architecture) }}

{{ $board := or .board "uefi" }}
{{ $repourl := or .repourl "https://images.apertis.org/ostree/repo" }}
{{ $osname := or .osname "apertis" }}
{{ $branch := or .branch (printf "%s/%s/%s-%s/%s" $osname $suite $architecture $board $type) }}
{{ $ostree := or .ostree "repo" }}

{{ $cmdline := or .cmdline "console=tty0 console=ttyS0,115200n8 rootwait rw quiet splash plymouth.ignore-serial-consoles fsck.mode=auto fsck.repair=yes systemd.gpt_auto=false" }}

{{ $demopack := or .demopack "disabled" }}

architecture: {{ $architecture }}

actions:
  - action: image-partition
    imagename: {{ $image }}.img
{{ if eq $type "fixedfunction" }}
    imagesize: 7G
{{ else }}
    imagesize: 15G
{{ end }}
    partitiontype: gpt

    mountpoints:
      - mountpoint: /
        partition: system
      - mountpoint: /boot/efi
        partition: EFI
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: EFI
        fs: vfat
        start: 0%
        end: 256M
        flags: [ boot ]
      - name: system
        fs: ext4
        start: 256M
{{ if eq $type "fixedfunction" }}
        end: 3000M
{{ else }}
        end: 6000M
{{ end }}
      - name: general_storage
        fs: ext4
{{ if eq $type "fixedfunction" }}
        start: 3000M
{{ else }}
        start: 6000M
{{ end }}
        end: 100%

  - action: ostree-deploy
    description: Deploying ostree onto image
    repository: {{ $ostree }}
    remote_repository: {{ $repourl }}
    branch: {{ $branch }}
    os: {{ $osname }}
    append-kernel-cmdline: {{ $cmdline }}
    {{ if .collection_id }}
    collection-id: {{ .collection_id }}
    {{ end }}

  - action: run
    description: "Enable signature verification"
    chroot: false
    command: ostree --repo="${ROOTDIR}/ostree/repo" config set 'remote "origin"'.sign-verify "true"

  - action: run
    description: "Enable update bundle verification"
    chroot: false
    command: ostree --repo="${ROOTDIR}/ostree/repo" config set core.sign-verify-deltas "true"

  # Add multimedia demo pack
  # Provide URL via '-t demopack:"https://images.apertis.org/media/multimedia-demo.tar.gz"'
  # to add multimedia demo files
{{ if ne $demopack "disabled" }}
  # Use wget to get some insight about https://phabricator.collabora.com/T11930
  # TODO: Revert to a download action once the cause is found
  - action: run
    description: Download multimedia demo pack
    chroot: false
    command: wget --debug {{ $demopack }} -O "${ARTIFACTDIR}/multimedia-demo.tar.gz"

  - action: unpack
    description: Unpack multimedia demo pack
    compression: gz
    file: multimedia-demo.tar.gz

  - action: run
    description: Clean up multimedia demo pack tarball
    chroot: false
    command: rm "${ARTIFACTDIR}/multimedia-demo.tar.gz"

  {{ if eq $type "hmi" }}
  - action: run
    description: Unpack Flatpak demo applications
    chroot: false
    command: tar xzf ${ARTIFACTDIR}/flatpak-demo-apps-{{ $architecture }}.tar.gz -C ${ROOTDIR}/ostree/deploy/{{ $osname }}/

  - action: run
    description: Clean up Flatpak demo applications tarball
    chroot: false
    command: rm ${ARTIFACTDIR}/flatpak-demo-apps-{{ $architecture }}.tar.gz
  {{ end }}
{{ end }}

  - action: run
    description: Install UEFI bootloader
    chroot: false
    script: scripts/setup-uefi-bootloader.sh {{ $osname }}

  - action: run
    description: List files on {{ $image }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $image }}.img.filelist.gz"

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create {{ $image }}.img > {{ $image }}.img.bmap

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f {{ $image }}.img

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: sha256sum {{ $image }}.img.gz > {{ $image }}.img.gz.sha256
