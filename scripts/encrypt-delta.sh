#!/bin/bash

set -eu

cleanup() {
    if [[ -v TMPDIR ]] ; then
        umount /dev/mapper/update
        rm -rf ${TMPDIR}
    fi
    if [[ -e /dev/mapper/update ]] ; then
        cryptsetup close update
    fi
}

trap "exit 1" HUP INT TERM
trap cleanup EXIT

DEVICE=
DELTA=
KEYFILE0=

# Fix Password-Based Key Derivation Function (PBKDF) memory cost to 64M for
# all kind of devices, preventing ENOMEM errors during decryption.
# The pbkdf-memory value is in kilobytes.
# Refs:
# https://github.com/P-H-C/phc-winner-argon2/blob/master/README.md
# https://wiki.php.net/rfc/argon2_password_hash
PBKDF_PARAMS="--pbkdf-memory 65536"

opts=$(getopt -o "d:" -l "device:,delta:,key:" -- "$@")
eval set -- "$opts"

i=0
while [[ $# -gt 0 ]]; do
    case $1 in
        -d|--device) DEVICE="$2"; shift 2;;
        --delta) DELTA="$2"; shift 2;;
        --key) eval KEYFILE${i}="$2"; i=$((${i} + 1)); shift 2;;
        --) shift; break;;
        *) ;;
    esac
done

if [[ -z "${DEVICE}" ]] || [[ -z "${DELTA}" ]] || [[ -z "${KEYFILE0}" ]] || [[ ${i} -gt 8 ]] || [[ $# -ne 0 ]] ; then
    echo "Please provide device, delta and key(s) (min 1, max 8)." >&2
    echo "Usage: $0 --device <dev> --delta <bundleFile> --key <keyFile> [--key <keyFile>]"
    exit 1
fi

# Initialize and open encrypted file,
cryptsetup luksFormat --type luks2 -q ${PBKDF_PARAMS} ${DEVICE} ${KEYFILE0}
cryptsetup open --type luks2 --key-file ${KEYFILE0} ${DEVICE} update

# Add supplementary keys
for i in {1..7}
do
    var="KEYFILE${i}"
    if [[ -v ${var} ]]
    then
        cryptsetup luksAddKey ${PBKDF_PARAMS} --key-file=${KEYFILE0} ${DEVICE} ${!var}
    fi
done

# Format filesystem
mkfs.ext2 -q -L update -m 0 -T largefile /dev/mapper/update

# Mount filesystem and copy files to it
TMPDIR=$(mktemp -d updateXXX)
mount /dev/mapper/update ${TMPDIR}
cp ${DELTA} ${TMPDIR}/static-update.bundle
sync

# Clean up is done automatically
