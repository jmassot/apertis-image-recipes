#!/bin/bash

set -eu

architecture=${1:-arm64}
mirror=${2:-https://repositories.apertis.org/apertis/}
osname=${3:-apertis}
suite=${4:-v2022pre}
target=${5:-imx8mn_bsh_smm_s2}

output_dir="$(pwd)/u-boot/${target}"

echo "Create chdist directory"
mkdir "${WORKSPACE}/chdist"

echo "Setup chdist"
chdist -d "${WORKSPACE}/chdist" create ${osname}-${suite} ${mirror} ${suite} target non-free

echo "Add Apertis gpg key to chdist"
cp "${WORKSPACE}/keyring/${osname}-archive-keyring.gpg" "${WORKSPACE}/chdist/${osname}-${suite}/etc/apt/trusted.gpg.d"

echo "Update repo (chdist)"
chdist -d "${WORKSPACE}/chdist" -a ${architecture} apt ${osname}-${suite} update

echo "Download U-Boot (chdist)"
chdist -d "${WORKSPACE}/chdist" -a ${architecture} apt ${osname}-${suite} download u-boot-imx

echo "Unpack U-Boot"
dpkg -x u-boot-imx_*.deb "${WORKSPACE}/deb-binaries"

echo "Download imx-firmware-ddr3 (chdist)"
chdist -d "${WORKSPACE}/chdist" -a ${architecture} apt ${osname}-${suite} download imx-firmware-ddr3

echo "Unpack imx-firmware-ddr3"
dpkg -x imx-firmware-ddr3_*.deb "${WORKSPACE}/deb-binaries"

echo "Create output dir"
mkdir -p "${output_dir}"

echo "Build U-Boot image"
ROOTDIR=${WORKSPACE} ${WORKSPACE}/scripts/imx8mn-uboot-image.sh -d "${target}" "${output_dir}/${target}_uboot.bin"

echo "Clean up"
rm -Rvf u-boot-imx_*.deb imx-firmware-ddr3_*.deb "${WORKSPACE}/deb-binaries" "${WORKSPACE}/chdist"
