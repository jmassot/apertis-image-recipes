#!/bin/sh

set -eu

if [ $# -ne 3 ]
then
  echo "Usage: $0 REPO PULL_URL BRANCH" >&2
  exit 1
fi

repo=$1
ostree_pull_url=$2
branch=$3

mkdir -p "${repo}"
ostree init --repo="${repo}" --mode archive-z2
ostree remote --repo="${repo}" add --no-gpg-verify origin "${ostree_pull_url}"

if ostree remote --repo=${repo} refs origin | grep -qw "origin:${branch}"; then
    echo "Pulling remote branch ${branch}"
    ostree pull --repo="${repo}" --depth=-1 --commit-metadata-only --disable-fsync origin "${branch}"
    ostree show --repo="${repo}" "${branch}"
else
    echo "No remote branch ${branch} found"
fi
