env set file /deb-binaries/usr/lib/u-boot/mx6qsabrelite/u-boot-dtb.imx
for d in 0 1; do
  if test -e mmc ${d}:1 ${file} ; then
    env set devnum ${d}
  fi
done

echo "Copying U-Boot from mmc ${devnum} to RAM"
if load mmc ${devnum}:1 0x12000000 ${file} ; then
  echo "Probing SPI-NOR"
  sf probe 0
  echo "Erasing SPI_NOR"
  sf erase 0x0 0xc0000
  echo "Writing U-Boot to SPI-NOR"
  sf write 0x12000000 0x400 0xc0000
  echo ""
  echo '+-----------------------------------------------------------------+'
  echo '|                  U-Boot installation complete                   |'
  echo '|                                                                 |'
  echo '| Please remove the SD Card and power cycle the board to continue |'
  echo '+-----------------------------------------------------------------+'
  loop 0 0
fi

echo '+-----------------------------------------------------------------+'
echo '|                  U-Boot installation FAILED (missing file?)     |'
echo '|                                                                 |'
echo '| Please check the SD Card and power cycle the board to continue  |'
echo '+-----------------------------------------------------------------+'
loop 0 0
