default_policy.policy.roles = {
  -- main sink
  ["Multimedia"]       = { ["priority"] = 0, ["action.default"] = "cork", ["alias"] = { "Movie", "Music", "Game" }, },
  ["GPS"]              = { ["priority"] = 5, ["action.default"] = "duck", },
  ["Phone"]            = { ["priority"] = 7, ["action.default"] = "cork", ["alias"] = { "CustomRingtone" }, },

  -- alert sink
  ["New_email"]        = { ["priority"] = 1, ["action.default"] = "mix", },
  ["Traffic_info"]     = { ["priority"] = 6, ["action.default"] = "mix", },
  ["Ringtone"]         = { ["priority"] = 7, ["action.default"] = "mix", },
}

default_policy.endpoints = {
  ["endpoint.multimedia"]       = { ["media.class"] = "Audio/Sink", ["role"] = "Multimedia", },
  ["endpoint.gps"]              = { ["media.class"] = "Audio/Sink", ["role"] = "GPS", },
  ["endpoint.phone"]            = { ["media.class"] = "Audio/Sink", ["role"] = "Phone", },
  ["endpoint.ringtone"]         = { ["media.class"] = "Audio/Sink", ["role"] = "Ringtone", },
  ["endpoint.new_email"]        = { ["media.class"] = "Audio/Sink", ["role"] = "New_email", },
  ["endpoint.traffic_info"]     = { ["media.class"] = "Audio/Sink", ["role"] = "Traffic_info", },
}
